from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import configparser
import ast
import os
import time

# how to install libraries:
# pip install configparser
# pip install selenium


class Timer:
    def __init__(self):
        self.start = time.time()

    def restart(self):
        self.start = time.time()

    def get_time_minutes(self):
        end = time.time()
        m, s = divmod(end - self.start, 60)
        h, m = divmod(m, 60)
        return int(h * 60 + m)


def message_replay(console_reply):
    t = time.localtime()
    print("[" + time.strftime("%H:%M:%S", t) + "]: "+ console_reply, end='\n')


def config():
    path_config = input(
        'Provide your location for chromedriver for example: /usr/bin/chromedriver ('
        'https://i.stack.imgur.com/bTBSt.png): remember you must have chromium/chrome installed: ')
    email_config = input('Email: ')
    password_config = input('Password: ')
    conversation_config = input(
        'provide the link to the chat you want to remove, must be messenger.com domain (for example: '
        'https://www.messenger.com/t/100000xxxxxxx) if you want to add more, separate the links with a comma: ')
    conversation_config = conversation_config.replace(' ', '').split(',')

    cp = configparser.ConfigParser()
    cp['configs'] = {}
    cp['configs']['path'] = path_config
    cp['configs']['email'] = email_config
    cp['configs']['password'] = password_config
    cp['configs']['conversations_links_original'] = str(conversation_config)
    cp['configs']['conversation_links_empty_chats_removed'] = str(conversation_config)
    with open('config.ini', 'w') as config_file:  # xd
        cp.write(config_file)
        print('config.ini saved, if you want to change it, remove it and restart the program')


def start():
    global driver
    global n
    n = 0
    driver = webdriver.Chrome(PATH)
    driver.get("https://messenger.com")
    time.sleep(2)
    driver.find_element_by_xpath("//button[text()='Zarządzaj ustawieniami danych']").click()
    time.sleep(2)
    driver.find_element_by_xpath("//button[text()='Akceptuję pliki cookie']").click()
    time.sleep(2)
    driver.find_element_by_id("email").send_keys(Login + Keys.TAB + Password + Keys.ENTER)
    time.sleep(5)
    driver.get(conversation)
    time.sleep(10)
    message_replay("Initialization completed")


def flush():
    message_replay("Flushing chromedriver")
    driver.quit()
    my_timer.restart()
    start()
    time.sleep(10)


def oznaczarka():  # Infinite loop for removing messages
    message_action = driver.find_elements_by_xpath("//div[contains(@aria-label, 'Message actions')]")
    for mes in reversed(message_action):
        try:
            mes.click()
            # print(mes)
            driver.find_element_by_xpath("//div[contains(@aria-label, 'More')]").click()
            driver.find_element_by_xpath("//div[contains(@aria-label, 'Remove Message')]").click()
            # time.sleep(0.18)
            driver.find_element_by_xpath('//span[text()="Remove"]').click()
            time.sleep(0.45)
        except:
            pass

    try:
        driver.execute_script(
            '''SCROLLER_QUERY = '[role="main"] .buofh1pr.j83agx80.eg9m0zos.ni8dbmo4.cbu4d94t.gok29vw1';MESSAGES_QUERY 
            = "[aria-label=Messages]";''')
        driver.execute_script('''document.querySelector(SCROLLER_QUERY).scrollTop = 0;''')
        driver.find_element_by_xpath("//i[contains(@class, 'hu5pjgll lzf7d6o1 sp_bxqiRnRQ4Q3 sx_da01b1')]").click()
        driver.find_element_by_xpath('//span[text()="Remove"]').click()
    except:
        pass

    try:
        driver.find_element_by_xpath("//div[@class='j83agx80 taijpn5t n851cfcs']")
        global n
        n = n + 1
        if n > 99:
            config = configparser.ConfigParser()
            config.read('config.ini')
            str_chats = config['configs']['conversation_links_empty_chats_removed']
            list_chats = ast.literal_eval(str_chats)
            del list_chats[0]
            message_replay('switching chat link')
            driver.get(list_chats[0])
            config['configs']['conversation_links_empty_chats_removed'] = str(list_chats)
            with open('config.ini', 'w') as configfile:
                config.write(configfile)
                configfile.close()
    except:
        pass

    try:
        driver.find_element_by_xpath('//span[text()="Remove"]').click()
    except:
        pass

def main():  # infinite loop, every 60 minutes restart the porgram
    while True:
        try:
            time_minutes = my_timer.get_time_minutes()
            oznaczarka()
            if time_minutes > 30:
                message_replay("Refreshing website...")
                # flush()
                # time.sleep(1200)
                # start()
                driver.switch_to.window(driver.window_handles[0])
                driver.get(list_chats[0])
                # print(my_timer.get_time_minutes())
                my_timer.restart()
                time.sleep(10)
                # driver.clos(driver.window_handles[1])
        except:
            pass


if os.path.isfile('config.ini') is True:  # Checking if file exists else create new config.ini file
    message_replay('config.ini file found, if you want to change it, remove it and restart the program')
    config = configparser.ConfigParser()
    config.read('config.ini')
    str_chats = config['configs']['conversation_links_empty_chats_removed']
    list_chats = ast.literal_eval(str_chats)
else:
    message_replay('config.ini file not found, creating new one')
    config()
    config = configparser.ConfigParser()
    config.read('config.ini')
    str_chats = config['configs']['conversation_links_empty_chats_removed']
    list_chats = ast.literal_eval(str_chats)

# Login variables
PATH = config['configs']['path']
Login = config['configs']['email']
Password = config['configs']['password']
conversation = list_chats[0]

# Executing main code and starting timer
my_timer = Timer()
start()
main()
